//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#include <stdio.h>
#include <stdlib.h>

#include "covidTrace.h"
#include "data/rawMacs.h"
#include "helpers/macros.h"

MACAddress BTNearMe() {
    uint8_t randIndex = RAND_RANGE(0, MAC_DATA_ARRAY_SIZE);
    return macaddr_new(macAddresses[randIndex]);
}

bool testCOVID() {
    bool trueOrFalse = RAND_RANGE(0, 2);
    return trueOrFalse;
}

void uploadContacts(Detection* detectedContacts, uint32_t length) {
    struct tm* timeInfo;
    time_t rawTime = time(NULL);
    timeInfo = localtime(&rawTime);

    char fileNameBuffer[255];
    char timestampBuffer[26];
    uint8_t header[3] = {0x42, 0x06, 0x99};
    strftime(timestampBuffer, 26, "%Y-%m-%d_%H:%M:%S", timeInfo);
    printf("%s\n", timestampBuffer);
    sprintf(fileNameBuffer, "RESULTS_UPLOAD_%s", timestampBuffer);
    printf("%s\n", fileNameBuffer);

    FILE* resultsFile = fopen(fileNameBuffer, "wb");

    fwrite(header, sizeof(header), 1, resultsFile);
    fwrite(detectedContacts, sizeof(Detection), length, resultsFile);

    fclose(resultsFile);
}
