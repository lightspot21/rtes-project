//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#include "helpers/CircularBuffer.h"
#include <stdlib.h>

void new_buf(cbuf_t * cb, size_t elements) {
    cb->dataBuf = (Detection*) malloc(elements * sizeof(Detection));
    cb->size = elements;
    cb->pos = 0;
}

void delete_buf(cbuf_t * cb) {
    free(cb->dataBuf);
}

void push(cbuf_t * cb, Detection d) {
    *(cb->dataBuf + cb->pos) = d;
    if (cb->pos < ((cb->size) - 1)) { // zero-based index
        cb->pos += 1;
    }
    else {
        cb->pos = 0;
        cb->isFull = true;
    }
}
bool full(cbuf_t* cb) {
    return cb->isFull;
}

void reset(cbuf_t * cb) {
    cb->pos = 0;
    cb->isFull = false;
}

// WILDLY UNSAFE YET EASIEST
Detection* get_buffer_state(cbuf_t* cb) {
    return cb->dataBuf;
}