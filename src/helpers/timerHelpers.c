//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#include "helpers/timerHelpers.h"

int create_periodic_timer(timer_t *timerId, struct sigevent *signalToSend, time_t triggerEveryKsecs, long triggerEveryKnsecs) {
    int timerCreateRet = timer_create(CLOCK_REALTIME, signalToSend, timerId);
    if (timerCreateRet == 0) {
        struct itimerspec timerSettings, oldTimerSettings;
        timerSettings.it_value.tv_nsec = triggerEveryKnsecs;
        timerSettings.it_value.tv_sec = triggerEveryKsecs;
        timerSettings.it_interval.tv_nsec = triggerEveryKnsecs;
        timerSettings.it_interval.tv_sec = triggerEveryKsecs;

        int timerSetRet = timer_settime(*timerId, 0, &timerSettings, &oldTimerSettings);
        if (timerSetRet != 0) {
            timer_delete(*timerId);
            return TIMER_START_FAIL;
        }
    }
    else {
        return TIMER_CREATE_FAIL;
    }
    return TIMER_OK;
}
