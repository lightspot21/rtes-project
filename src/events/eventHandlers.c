//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#include <stdio.h>
#include "events/eventHandlers.h"
#include "covidTrace.h"

// Pick a random MAC address, find if it's already seen, and if it is
void onBTNearMe() {
    Detection d;
    new_detection(&d, BTNearMe());
    Detection* buf_state = get_buffer_state(&detectionsBuffer);
    unsigned long latestLogIndex;
    if (detectionsBuffer.pos == 0) {
        latestLogIndex = (detectionsBuffer.size - 1);
    }
    else {
        latestLogIndex = detectionsBuffer.pos - 1;
    }
    int i = (int) latestLogIndex; // cast is valid iff latestLogIndex is well below INT_MAX
    do {
        if (i >= 0) {
            time_t deltaTime = d.detectionTime - buf_state[i].detectionTime;
            // 4 - 20min (in seconds)
            if (((deltaTime > 240) && (deltaTime < 1200)) && macaddr_identical(&d.macAddress, &(buf_state->macAddress))) {
                push(&closeContactsBuffer, d);
                printf("Close contact detected\n");
            }
            i--;
        }
        else {
            i = (int) detectionsBuffer.size - 1;
        }
    } while (i != (int) latestLogIndex);
    printf("New MAC detected\n");
    push(&detectionsBuffer, d);
}

void onDetectionClear() {
    printf("Detection buffer cleared\n");
    reset(&closeContactsBuffer);
}

void onCovidTest() {
    bool isPositive = testCOVID();
    printf("COVID test status %d\n", isPositive);
    if (isPositive) {
        printf("Positive test detected - uploading\n");
        uploadContacts(get_buffer_state(&closeContactsBuffer), closeContactsBuffer.pos);
    }
}

#ifdef DEBUG_MODE

void onPrintf() {
//    char test[50];
//    macaddr_to_string(test, &latestMacAddress);
//    printf("%s\n", test);
}
#endif
