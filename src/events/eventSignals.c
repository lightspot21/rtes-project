//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#include "events/eventSignals.h"
#include "events/eventHandlers.h"

sigevent_t commonSignal;

sigevent_t nearMeSignal;
sigevent_t detectionClearSignal;
sigevent_t covidTestSignal;

#ifdef DEBUG_MODE
sigevent_t printfSignal;
#endif

void init_event_signals(pthread_attr_t *attr) {
    commonSignal.sigev_notify = SIGEV_THREAD;
    commonSignal.sigev_value.sival_int = 20;
    commonSignal.sigev_notify_attributes = attr;

    nearMeSignal = commonSignal;
    nearMeSignal.sigev_notify_function = onBTNearMe;

    detectionClearSignal = commonSignal;
    detectionClearSignal.sigev_notify_function = onDetectionClear;

    covidTestSignal = commonSignal;
    covidTestSignal.sigev_notify_function = onCovidTest;

#ifdef DEBUG_MODE
    printfSignal = commonSignal;
    printfSignal.sigev_notify_function = onPrintf;
#endif
}
