//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#include <stdint.h>
#include <stdio.h>

#include "datatypes/MACAddress.h"

bool macaddr_identical(MACAddress *mac1, MACAddress *mac2) {
    for (uint8_t index = 0; index < MAC_LENGTH_BYTES; index++) {
        if (mac1->bytes[index] != mac2->bytes[index]) {
            return false;
        }
    }
    return true;
}

MACAddress macaddr_new(char *addr_str) {
    MACAddress parsedMac;
    int result = sscanf(addr_str, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx*c",
           &parsedMac.bytes[0],
           &parsedMac.bytes[1],
           &parsedMac.bytes[2],
           &parsedMac.bytes[3],
           &parsedMac.bytes[4],
           &parsedMac.bytes[5]);
    if (result != 6) {
        parsedMac.validMac = false;
        fputs("Invalid MAC address", stderr);
        return parsedMac;
    }
    parsedMac.validMac = true;
    return parsedMac;
}

void macaddr_to_string(char* buffer, MACAddress* macAddress) {
    sprintf(buffer, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
            macAddress->bytes[0],
            macAddress->bytes[1],
            macAddress->bytes[2],
            macAddress->bytes[3],
            macAddress->bytes[4],
            macAddress->bytes[5]);
}
