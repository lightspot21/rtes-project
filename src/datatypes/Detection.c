//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#include "datatypes/Detection.h"

void new_detection(Detection* detection, MACAddress macAddress) {
    detection->macAddress = macAddress;
    detection->detectionTime = time(NULL);
}
