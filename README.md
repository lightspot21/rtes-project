# RTES Course Project @ ECE AUTh - October 2021

## What is this about?
This repository contains the code pertaining to the final project of the
"Real-Time Embedded Systems" course taught at the Electrical and Computer
Engineering major of the Polytechnic School of the Aristotle University of
Thessaloniki (AUTh). In short, it is a rough simulation of the work a
contact tracing application does (like the ones developed for combating the
ongoing COVID-19 pandemic). Reviews - comments always welcome.

# Building
Tested at CMake v3.20+, although most logically any version after 3.1 should work.
Run:
- `cmake -B <build-files-directory> -S .`
- `make` (while inside the build file directory)

This program uses `pthreads` and `librt`. A suitable toolchain file is also provided
for Raspberry Pi targets. 

# License
GNU GPL v3 or later.