//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#ifndef ESPX_PROJECT_COVIDTRACE_H
#define ESPX_PROJECT_COVIDTRACE_H

#include <stdint.h>

#include "datatypes/MACAddress.h"
#include "helpers/CircularBuffer.h"

extern cbuf_t detectionsBuffer;
extern cbuf_t closeContactsBuffer;

MACAddress BTNearMe();
bool testCOVID();
void uploadContacts(Detection* detectedContacts, uint32_t length);


#endif//ESPX_PROJECT_COVIDTRACE_H
