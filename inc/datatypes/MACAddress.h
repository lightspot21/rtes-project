//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#ifndef ESPX_PROJECT_MACADDRESS_H
#define ESPX_PROJECT_MACADDRESS_H

#include <stdbool.h>
#include <stdint.h>
#define MAC_LENGTH_BYTES 6
#define MAC_STRING_REPRO_SIZE 18 //bytes, includes null

typedef struct {
    uint8_t bytes[MAC_LENGTH_BYTES];
    bool validMac;
} MACAddress;
/*
 * Checks if 2 addresses are the same. Doesn't check if the addresses are valid
 */
bool macaddr_identical(MACAddress* mac1, MACAddress* mac2);

/*
 * Parses a MAC address string.
 * "Checks" if the given address is valid
 */
MACAddress macaddr_new(char* s);

/*
 * Creates string representation of the MAC address
 */
void macaddr_to_string(char* buffer, MACAddress* macAddress);

#endif//ESPX_PROJECT_MACADDRESS_H
