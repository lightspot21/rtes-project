//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#ifndef ESPX_PROJECT_DETECTION_H
#define ESPX_PROJECT_DETECTION_H

#include "datatypes/MACAddress.h"
#include <time.h>

typedef struct Detection {
    MACAddress macAddress;
    time_t detectionTime;
} Detection;

void new_detection(Detection* detection, MACAddress macAddress);

#endif//ESPX_PROJECT_DETECTION_H
