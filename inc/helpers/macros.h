//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#ifndef ESPX_PROJECT_MACROS_H
#define ESPX_PROJECT_MACROS_H

// random numbers in [start, end)
#define RAND_RANGE(start, end) (rand() % (end - start)) + start

#endif//ESPX_PROJECT_MACROS_H
