//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//
#ifndef ESPX_PROJECT_TIMERHELPERS_H
#define ESPX_PROJECT_TIMERHELPERS_H

#include <signal.h>
#include <time.h>

enum TimerCreation {
    TIMER_OK,
    TIMER_CREATE_FAIL,
    TIMER_START_FAIL
};

/*
 * Creates a seconds-based periodic timer.
 * Timer fires for the first time after triggerEveryKnsecs nanoseconds, so be
 * careful for off-by-one errors in case fires are counted.
 * e.g. if total time is 10n sec, with a timer period of 1 nsec, only 9 fires
 * will be registered.
 *
 * Returns TIMER_CREATE_FAIL or TIMER_START_FAIL in case of any such errors
 */
int create_periodic_timer(timer_t *timerId, struct sigevent *signalToSend, time_t triggerEveryKsecs, long triggerEveryKnsecs);

#endif//ESPX_PROJECT_TIMERHELPERS_H
