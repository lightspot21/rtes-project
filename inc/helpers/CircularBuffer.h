//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#ifndef ESPX_PROJECT_CIRCULARBUFFER_H
#define ESPX_PROJECT_CIRCULARBUFFER_H

#include "datatypes/Detection.h"

typedef struct cbuf_t {
    Detection* dataBuf;
    size_t size;
    size_t pos;
    bool isFull;
} cbuf_t;

void new_buf(cbuf_t * cb, size_t elements);
void delete_buf(cbuf_t * cb);
bool full(cbuf_t* cb);
void push(cbuf_t * cb, Detection d);
void reset(cbuf_t * cb);
Detection* get_buffer_state(cbuf_t* cb);

#endif//ESPX_PROJECT_CIRCULARBUFFER_H
