//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#ifndef ESPX_PROJECT_RAWMACS_H
#define ESPX_PROJECT_RAWMACS_H

#define MAC_DATA_ARRAY_SIZE (sizeof(macAddresses)/sizeof(macAddresses[0]))

static char* macAddresses[] = {
        "e6:6d:b3:7e:ec:4f",
        "a3:79:0a:fa:a7:88",
        "4b:58:96:70:23:53",
        "c8:f1:d7:e1:94:11",
        "6f:96:50:92:63:90",
        "ee:f5:39:fd:ac:72",
        "f7:ba:c5:6d:2b:58",
        "a9:29:a6:cb:ba:49",
        "74:1b:86:92:80:6f",
        "d6:51:e1:e2:86:7e",
        "3a:1d:b0:d6:96:0e",
        "0d:06:67:f6:e6:f9",
        "39:2b:d9:df:3a:59",
        "b9:b2:dd:2b:c1:cf",
        "02:17:e3:e7:af:4a",
        "1b:66:40:72:8a:16",
        "69:9d:bf:61:0f:43",
        "7a:03:7f:61:de:2f",
        "19:db:30:e7:5d:b4",
        "86:4f:6a:2d:a9:33",
        "e5:98:a4:78:f3:23",
        "df:1d:99:84:35:33",
        "56:10:76:8c:3e:7b",
        "db:c6:ea:6e:aa:e6",
        "0b:e2:b3:30:08:88",
        "35:fe:59:dd:00:9c",
        "19:e5:8d:57:01:19",
        "12:6d:f2:22:ad:ee",
        "9b:32:bc:34:7b:85",
        "21:1c:27:d4:a0:36",
        "ef:99:f0:4a:de:d3",
        "3c:e5:10:71:47:7f",
        "0c:f3:f5:85:64:6a",
        "0d:44:88:b4:1d:7b",
        "8a:40:38:86:6d:9b",
        "0e:86:de:20:bb:2c",
        "1e:c4:70:75:68:d0",
        "cb:92:d7:5c:2c:57",
        "70:13:0e:a2:6e:97",
        "ce:5f:ee:fb:6e:96",
        "6f:a3:20:84:19:c4",
        "cd:04:36:3d:ec:8d",
        "e8:21:6e:ca:17:05",
        "81:02:79:b7:d1:6b",
        "e6:51:b9:31:6b:37",
        "19:0b:2e:65:1d:85",
        "76:ce:a9:8c:ca:58",
        "6f:43:62:ac:e8:9e",
        "6d:5f:56:89:4c:e5",
        "4f:68:e6:29:d8:b0",
};

#endif//ESPX_PROJECT_RAWMACS_H
