//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#ifndef ESPX_PROJECT_CONSTANTS_H
#define ESPX_PROJECT_CONSTANTS_H

// 22 mins (20 + 10% margin) * 6 detections/min
#define DETECTIONS_BUFFER_SIZE_ELEMENTS 132

// worst case: all the time the same MAC. 6 det/hr * 24 hr * 14d
// this makes sure we'll never run out of buffer space (aka the
// circular buffer will never wrap around)
#define CLOSE_CONTACTS_BUFFER_SIZE_ELEMENTS 2016

// helpful time constants to keep same timebase but also apply required time scaling
// (from 30 days to 7 hrs)
#define SECOND_IN_NSEC 999999999ul //max supported value for a single second according to docs
#define HOUR_IN_SECONDS 3600ul
#define DAY_IN_HOURS 24ul
#define DAY_IN_SECONDS HOUR_IN_SECONDS * DAY_IN_HOURS

#endif//ESPX_PROJECT_CONSTANTS_H
