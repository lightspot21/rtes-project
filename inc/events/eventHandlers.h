//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#ifndef ESPX_PROJECT_EVENTHANDLERS_H
#define ESPX_PROJECT_EVENTHANDLERS_H

void onBTNearMe();
void onDetectionClear();
void onCovidTest();

#ifdef DEBUG_MODE
void onPrintf();
#endif

#endif//ESPX_PROJECT_EVENTHANDLERS_H
