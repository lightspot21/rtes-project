//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#ifndef ESPX_PROJECT_EVENTSIGNALS_H
#define ESPX_PROJECT_EVENTSIGNALS_H

#include <signal.h>
#include <pthread.h>

extern sigevent_t nearMeSignal;
extern sigevent_t detectionClearSignal;
extern sigevent_t covidTestSignal;

#ifdef DEBUG_MODE
extern sigevent_t printfSignal;
#endif

void init_event_signals(pthread_attr_t *attr);
#endif//ESPX_PROJECT_EVENTSIGNALS_H
