//
// Pavlakis Grigorios (AEM 9571) <grigpavl@ece.auth.gr>
// All rights reserved
//

#include <stdlib.h>
#include <unistd.h>
#include "covidTrace.h"
#include "events/eventSignals.h"
#include "helpers/timerHelpers.h"
#include "data/constants.h"

cbuf_t detectionsBuffer;
cbuf_t closeContactsBuffer;

int main(void) {
    // initialize RNG and pthreads
    srand(time(NULL));
    pthread_attr_t threadSettings;
    pthread_attr_init(&threadSettings);

    // initialize buffers
    new_buf(&detectionsBuffer, DETECTIONS_BUFFER_SIZE_ELEMENTS);
    new_buf(&closeContactsBuffer, CLOSE_CONTACTS_BUFFER_SIZE_ELEMENTS);

    // set handlers to the highest priority
    struct sched_param schedulerParams;
    schedulerParams.sched_priority = 99;
    pthread_attr_setschedparam(&threadSettings, &schedulerParams);
    pthread_attr_setschedpolicy(&threadSettings, SCHED_RR); // round-robin "real-time" (SCHED_OTHER is default behavior)

    // initialize all signals
    init_event_signals(&threadSettings);

    // make timers and start them
    timer_t nearMeTimer, detectionClearTimer, covidTestTimer;

    // time starts right after invocation of each function
    create_periodic_timer(&nearMeTimer, &nearMeSignal, 10, 0);
    create_periodic_timer(&detectionClearTimer, &detectionClearSignal, 14 * DAY_IN_SECONDS, 0);
    create_periodic_timer(&covidTestTimer, &covidTestSignal, 4*HOUR_IN_SECONDS,0);
    sleep(30 * DAY_IN_SECONDS);
}
